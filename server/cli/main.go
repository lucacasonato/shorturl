package main

import (
	"fmt"
	"time"

	"gitlab.com/creativeguy2013/shorturl/server/api"
	survey "gopkg.in/AlecAivazis/survey.v1"
)

var qs = []*survey.Question{
	{
		Name:     "hours",
		Prompt:   &survey.Input{Message: "How many hours should the new API key be valid for? (duration eg. 3h for 3 hours)"},
		Validate: survey.Required,
	},
}

func main() {
	answers := struct {
		Hours string // survey will match the question and field names
	}{}

	err := survey.Ask(qs, &answers)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	t, err := time.ParseDuration(answers.Hours)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	token, err := api.TokenCreate(time.Now().Add(t))
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("Your token is: " + token)
}
