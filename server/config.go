package main

import (
	"encoding/json"
	"os"

	"gitlab.com/creativeguy2013/shorturl/server/ui"
)

type config struct {
	Domain          string `json:"domain"`
	Port            string `json:"port"`
	DefaultRedirect string `json:"default_redirect"`
}

func loadConfig() (cfg config) {
	ui.Debug("[config] starting load")
	file, err := os.Open("./config.json")
	if err != nil {
		ui.Error("[config] could not open file (" + err.Error() + ")")
	}

	ui.Debug("[config] opened file")
	err = json.NewDecoder(file).Decode(&cfg)
	if err != nil {
		ui.Error("[config] could not decode file (" + err.Error() + ")")
	}

	ui.Debug("[config] finished decode")
	ui.Debug("[config] loaded")
	return
}
