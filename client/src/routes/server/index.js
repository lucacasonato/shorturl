import React from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

import { ShortURLList } from '../../components/shortUrl/list';
import { ShortURLAdd } from '../../components/shortUrl/add';

export class ServerRoute extends React.Component {
    render() {
        return (
            <Switch>
                <Route path="/server/:server/add" render={(r) => <ShortURLAdd server={this.props.server} />} />
                <Route path="/server/:server/list" exact render={(r) => <Redirect to={`/server/${r.match.params.server}/list/1`} />} />
                <Route path="/server/:server/list/:page" render={(r) => <ShortURLList server={this.props.server} serverRemove={() => this.props.serverRemove()} page={r.match.params.page} />} />
                <Route path="/server/:server" render={(r) => <Redirect to={`/server/${r.match.params.server}/list`} />} />
            </Switch>
        )
    }
}