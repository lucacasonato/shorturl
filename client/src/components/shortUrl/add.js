import React from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

import shortid from "shortid";

import { Container, Input, Button, Form, FormGroup, Label, Alert } from 'reactstrap';

export class ShortURLAdd extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            short: "",
            long: "",
            advanced: false,
            done: false
        }
    }

    submit(e) {
        e.preventDefault()

        if (this.state.long !== "") {

            var short = this.state.short ? this.state.short : shortid.generate()

            axios.post(`${this.props.server.domain}/api/v1/shorturls`, {
                long: this.state.long,
                short
            }, {
                headers: {
                    "Authorization": `Bearer ${this.props.server.token}`
                }
            }).then(resp => {
                console.log(resp)
                if (resp.status == 204) {
                    this.setState({done: true})
                }
            }).catch(err => {
                console.error(err)
            })
    
        }
    }

    render() {
        if (this.state.done) {
            return <Redirect to={`/server/${this.props.server.id}`} />
        }

        return (
            <>
                <Container>
                    <br />
                    <h2>{this.props.server.domain}</h2>
                    <br />
                    <Form style={{ margin: "0 auto" }} onSubmit={(e) => this.submit(e)}>
                        <FormGroup>
                            <Label for="long">Long URL</Label>
                            <Input type="url" name="long" id="long" placeholder="https://cdn.antipy.com/downloads/linux/installer/latest" value={this.state.long} onChange={e => this.setState({ long: e.target.value })} />
                        </FormGroup>
                        {this.state.advanced ? (
                            <FormGroup>
                                <Label for="short">Short Tag</Label>
                                <Input type="text" name="short" id="short" placeholder="linuxdl" value={this.state.short} onChange={e => this.setState({ short: e.target.value })} />
                            </FormGroup>
                        ) : (
                                <>
                                    <a onClick={() => this.setState({ advanced: !this.state.advanced })} href="#advanced">Advanced</a>
                                    <br />
                                    <br />
                                </>
                            )}
                        <Button>Add Short URL</Button>
                    </Form>
                </Container>
            </>
        )
    }
}