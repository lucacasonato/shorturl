import React from 'react';
import { Link } from 'react-router-dom';

import { Container, ListGroup, ListGroupItem } from 'reactstrap';

export class ServerSelect extends React.Component {
    render() {
        return (
            <Container style={{ textAlign: "center" }}>
                <br />
                <h1>Welcome to Short URL</h1>
                <p>Select a server below or <Link to="/add">add</Link> another one...</p>
                {this.props.servers.length > 0 ? (
                    <ListGroup style={{ textAlign: "left" }}>
                        {this.props.servers.map(server => {
                            return (
                                <ListGroupItem tag={Link} to={`/server/${server.id}`}>{server.domain}</ListGroupItem>
                            )
                        })}
                    </ListGroup>
                ) : (
                    <h5><Link to="/add">Add</Link> your first server...</h5>
                )}
            </Container>
        )
    }
}