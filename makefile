run:
	cd server \
	go build main.go -o ../shorturl
	./shorturl

caddy:
	caddy -conf Caddyfile